﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQ_HW
{
    class Program
    {
        static void Main(string[] args)
        {
            //Содержит 24 гласных букв
            string quote = "Не тот велик, кто никогда не падал, а тот велик — кто падал и вставал!\n — Конфуций";
            Console.WriteLine(quote);
            Console.WriteLine($"\nВсего гласных: {quote.CountVowels()}");
            Console.WriteLine($"\nТранслитерация:\n{quote.Transliteration()}");
        }
    }

    static class StringExtentsions
    {
        //Ленивая загрузка
        static readonly Lazy<char[]> vowels = new Lazy<char[]>
            (()=> new char[]{ 'А','а', 'О','о', 'И','и', 'Е','е', 'Ё','ё', 'Э','э', 'Ы','ы', 'У','у', 'Ю','ю', 'я','я' });

        static Dictionary<char, string> transliterationChars = new Dictionary<char, string>
            { 
                ['А'] = "A", 
                ['Б'] = "B",
                ['В'] = "V",
                ['Г'] = "G",
                ['Д'] = "D",
                ['Е'] = "Е",
                ['Ё'] = "YO",
                ['Ж'] = "ZH",
                ['З'] = "Z",
                ['И'] = "I",
                ['Й'] = "Y",
                ['К'] = "K",
                ['Л'] = "L",
                ['М'] = "M",
                ['Н'] = "N",
                ['О'] = "O",
                ['П'] = "P",
                ['Р'] = "R",
                ['С'] = "S",
                ['Т'] = "T",
                ['У'] = "U",
                ['Ф'] = "F",
                ['Х'] = "KH",
                ['Ц'] = "TS",
                ['Ч'] = "CH",
                ['Ш'] = "SH",
                ['Щ'] = "SCH",
                ['Ъ'] = "",
                ['Ы'] = "Y",
                ['Ь'] = "",
                ['Э'] = "E",
                ['Ю'] = "YU",
                ['Я'] = "YA"
            };

        static StringExtentsions()
        {
            //Создаём пару для букв нижнего регистра
            int size = transliterationChars.Count;
            for (int i = 0; i < size; i++)
            {
                KeyValuePair<char, string> kvp = transliterationChars.ElementAt(i);
                transliterationChars.Add(char.ToLower(kvp.Key), kvp.Value.ToLower());
            }
        }

        public static int CountVowels(this string str)
        {
            return  str?.Where((c) => vowels.Value.Contains(c)).Count() ?? 0;
        }

        public static string Transliteration(this string text)
        {
            StringBuilder sb = new StringBuilder();
            Translit(text).ForEach((c) => sb.Append(c));
            return sb.ToString();
        }

        //закрепляем yield return
        private static IEnumerable<string> Translit(string text)
        {
            string str;
            var tchrs = transliterationChars;
            foreach (char c in text)
            {
                str = tchrs.ContainsKey(c) ? tchrs[c] : c.ToString();
                yield return str;
            }
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (action == null) return;

            foreach (T element in source)
                action(element);
        }

    }
}
